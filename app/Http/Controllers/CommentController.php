<?php

namespace App\Http\Controllers;

use App\comment;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $comments = comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comments
        ], 200);
    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $comment = comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment
        ], 200);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id'  => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = comment::create([
            'content' => $request->content,
            'post_id'  => $request->post_id,
        ]);


        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comment));



        // //ini dikirim kepada yang memiliki post
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
        // //ini dikirim kepada yang memiliki comment
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        //success save to database
        if ($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($allRequest, [
            'content' => 'required',
            'post_id'  => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = comment::findOrFail($id);

        if ($comment) {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            //update post
            $comment->update([
                'content' => $request->content,
                'post_id'  => $request->post_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $comment = comment::findOrfail($id);

        if ($comment) {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            //delete post
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
