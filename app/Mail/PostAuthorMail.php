<?php

namespace App\Mail;

use App\comment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Tymon\JWTAuth\Claims\Subject;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostAuthorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comment.post_author_mail')
            ->subject('Full Stack Web PKS DS');
    }
}
